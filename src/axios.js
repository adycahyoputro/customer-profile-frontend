import axios from 'axios'

const instance = axios.create({
  //server local
  // baseURL: 'http://localhost:8002'
  //server ssh ady
  baseURL: 'http://165.22.55.132:8002',
  //server ssh mas zulfikar
  // baseURL: 'http://165.22.55.132:8001'
})

export default instance
