import React, { lazy, useEffect, useState } from 'react'

import {
  CAvatar,
  CButton,
  CButtonGroup,
  CCard,
  CCardBody,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CTable,
  CTableBody,
  CTableDataCell,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
} from '@coreui/react'

import axios from '../../axios'
import { useHistory } from 'react-router-dom'
import DetailUser from './DetailUser'

const ListUser = () => {
  let history = useHistory()
  const [userList, setUserList] = useState([])
  // const [currentId, setCurrentId] = useState(null);
  const [fetchStatus, setFetchStatus] = useState(true)
  const [visible, setVisible] = useState(false)

  useEffect(() => {
    const fetchData = async () => {
      try {
        const { data } = await axios.get(`/users`)
        let result = data.data.map((res) => {
          let { userid, name, age } = res
          return {
            userid,
            name,
            age,
          }
        })
        console.log(result)
        setUserList([...result])
      } catch (error) {
        alert('file failed show')
        console.log(error)
      }
    }
    if (fetchStatus) {
      fetchData()
      setFetchStatus(false)
    }
  }, [fetchStatus, setFetchStatus])

  // const handleInsertUser = () => {
  //   history.push('/create-user')
  // }

  const handleDetail = (event) => {
    let idUser = parseInt(event.target.value);
    localStorage.setItem("idUser", idUser);
    history.push(`/user/${idUser}`)
    // setVisible(!visible)
  }

  return (
    <>
      <CCard className="mb-4">
        <CCardBody>
          <h1>USER LIST</h1>
        {/* <CButton color="info" variant="outline" onClick={handleInsertUser}>Create User</CButton> */}
          <CTable hover>
            <CTableHead>
              <CTableRow>
                <CTableHeaderCell scope="col">No</CTableHeaderCell>
                <CTableHeaderCell scope="col">ID</CTableHeaderCell>
                <CTableHeaderCell scope="col">Name</CTableHeaderCell>
                <CTableHeaderCell scope="col">Age</CTableHeaderCell>
                <CTableHeaderCell scope="col">Action</CTableHeaderCell>
              </CTableRow>
            </CTableHead>
            <CTableBody>
              {userList !== null && (
                <>
                  {userList.map((val, index) => {
                    return (
                      <CTableRow key={index}>
                        <CTableHeaderCell scope="row">{index + 1}</CTableHeaderCell>
                        <CTableDataCell>{val.userid}</CTableDataCell>
                        <CTableDataCell>{val.name}</CTableDataCell>
                        <CTableDataCell>{val.age}</CTableDataCell>
                        <CTableDataCell>
                          <CButton color="info" variant="ghost" onClick={handleDetail} value={val.userid}>
                            Detail
                          </CButton>
                        </CTableDataCell>
                      </CTableRow>
                    )
                  })}
                </>
              )}
            </CTableBody>
          </CTable>
        </CCardBody>
      </CCard>
      <CModal alignment="center" visible={visible} onClose={() => setVisible(false)}>
        <CModalHeader>
          <CModalTitle>Risk Profile</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <DetailUser/>
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => setVisible(false)}>
            Close
          </CButton>
          <CButton color="primary">Save changes</CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

export default ListUser