import React, { useEffect, useState } from 'react'

import {
  CButton,
  CCard,
  CCardBody,
  CTable,
  CTableBody,
  CTableDataCell,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
} from '@coreui/react'

import axios from '../../axios'
import { useHistory } from 'react-router-dom'
import Cookies from "js-cookie";

const DetailUser = () => {
  let history = useHistory()
  const [detailUserList, setDetailUserList] = useState([])
  const [fetchStatus, setFetchStatus] = useState(true)
  var idUser = localStorage.getItem("idUser")

  useEffect(() => {
    const fetchData = async () => {
      try {
        const {data} = await axios.get(`/user/${idUser}`,{
          headers: {"token" : Cookies.get('token')}
        })
        let result = data.data
        console.log(result)
        setDetailUserList({
          // id : result.Id,
          id : result.id,
          // userid : result.userid,
          mm : result.mm,
          bond : result.bond,
          stock : result.stock
        })
      } catch (error) {
        alert('file failed show')
        console.log(error)
      }
    }
    if (fetchStatus) {
      fetchData()
      setFetchStatus(false)
    }
  }, [fetchStatus, setFetchStatus])
  console.log(detailUserList)

  const handleBackToList = () => {
    history.push('/user/list')
  }
  return (
    <>
      <CCard className="mb-4">
        <CCardBody>
          <CTable hover>
            <CTableHead>
              <CTableRow>
                {/* <CTableHeaderCell scope="col">No</CTableHeaderCell> */}
                <CTableHeaderCell scope="col">ID</CTableHeaderCell>
                {/* <CTableHeaderCell scope="col">User ID</CTableHeaderCell> */}
                <CTableHeaderCell scope="col">MM (%)</CTableHeaderCell>
                <CTableHeaderCell scope="col">Bond (%)</CTableHeaderCell>
                <CTableHeaderCell scope="col">Stock (%)</CTableHeaderCell>
              </CTableRow>
            </CTableHead>
            <CTableBody>
                      <CTableRow>
                        {/* <CTableHeaderCell scope="row">{1}</CTableHeaderCell> */}
                        <CTableDataCell>{detailUserList.id}</CTableDataCell>
                        {/* <CTableDataCell>{detailUserList.userid}</CTableDataCell> */}
                        <CTableDataCell>{detailUserList.mm}</CTableDataCell>
                        <CTableDataCell>{detailUserList.bond}</CTableDataCell>
                        <CTableDataCell>{detailUserList.stock}</CTableDataCell>
                      </CTableRow>
            </CTableBody>
          </CTable>
          <CButton color="info" variant="outline" onClick={handleBackToList}>Back to List</CButton>
        </CCardBody>
      </CCard>
    </>
  )
}

export default DetailUser