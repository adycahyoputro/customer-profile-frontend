import React from 'react'
import { Link } from 'react-router-dom'
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CFormInput,
  CInputGroup,
  CInputGroupText,
  CRow,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { cilLockLocked, cilUser } from '@coreui/icons'
import axios from '../../../axios'
import { useHistory } from 'react-router-dom'

const CreateUser = () => {
  let history = useHistory()
  const [username, setUsername] = React.useState('')
  const [age, setAge] = React.useState(0)
  const [password, setPassword] = React.useState('')
  const [error, setError] = React.useState('')
  const handleInsertUser = (e) => {
    e.preventDefault()
    console.log('username :', username)
    console.log('password :', password)
    axios
      .post(`/user`, {
        name: username,
        age: parseInt(age),
        password: password,
      })
      .then((res) => {
        if (res.data.success) {
          localStorage.setItem('user', JSON.stringify(res.data.data))
        }
        history.push("/");
      })
      .catch((error) => {
        setError(error)
        console.log(error)
      })
  }
  return (
    <div className="bg-light min-vh-100 d-flex flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md={6}>
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <CForm onSubmit={handleInsertUser}>
                    <h1>Create User</h1>
                    <p className="text-medium-emphasis">Insert User In to your account</p>
                    <CInputGroup className="mb-3">
                      <CInputGroupText>
                        <CIcon icon={cilUser} />
                      </CInputGroupText>
                      <CFormInput
                        placeholder="Username"
                        autoComplete="username"
                        onChange={(e) => setUsername(e.target.value)}
                        value={username}
                        required
                      />
                    </CInputGroup>
                    <CInputGroup className="mb-3">
                      <CInputGroupText>
                        <CIcon icon={cilUser} />
                      </CInputGroupText>
                      <CFormInput
                        type="number"
                        placeholder="Age"
                        autoComplete="age"
                        onChange={(e) => setAge(e.target.value)}
                        value={age}
                        required
                      />
                    </CInputGroup>
                    <CInputGroup className="mb-4">
                      <CInputGroupText>
                        <CIcon icon={cilLockLocked} />
                      </CInputGroupText>
                      <CFormInput
                        type="password"
                        placeholder="Password"
                        autoComplete="current-password"
                        onChange={(e) => setPassword(e.target.value)}
                        value={password}
                        required
                      />
                    </CInputGroup>
                    <CRow>
                      <CCol xs={6}>
                        <CButton color="primary" className="px-4" type="submit">
                          Submit
                        </CButton>
                      </CCol>
                    </CRow>
                  </CForm>
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
}

export default CreateUser
