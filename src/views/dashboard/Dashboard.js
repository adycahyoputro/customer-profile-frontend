import React from 'react'

import Cookies from "js-cookie";

const Dashboard = () => {

  return (
    <>
      <h1>Selamat Datang {Cookies.get('username')}</h1>
    </>
  )
}

export default Dashboard
