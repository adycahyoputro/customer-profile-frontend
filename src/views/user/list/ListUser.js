import React, { lazy, useEffect, useState } from 'react'

import {
  CButton,
  CButtonGroup,
  CCard,
  CCardBody,
  CTable,
  CTableBody,
  CTableDataCell,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
} from '@coreui/react'

import axios from '../../../axios'
import { useHistory } from 'react-router-dom'
import Cookies from "js-cookie";

const ListUser = () => {
  let history = useHistory()
  const [userList, setUserList] = useState([])
  // const [currentId, setCurrentId] = useState(null);
  const [fetchStatus, setFetchStatus] = useState(true)

  useEffect(() => {
    const fetchData = async () => {
      try {
        const { data } = await axios.get(`/users`, {
          headers: {"token" : Cookies.get('token')}
        })
        let result = data.data.map((res) => {
          let { id, name, age } = res
          return {
            id,
            name,
            age,
          }
        })
        console.log(result)
        setUserList([...result])
      } catch (error) {
        alert('file failed show')
        console.log(error)
      }
    }
    if (fetchStatus) {
      fetchData()
      setFetchStatus(false)
    }
  }, [fetchStatus, setFetchStatus])

  const handleDetail = (event) => {
    let idUser = parseInt(event.target.value);
    localStorage.setItem("idUser", idUser);
    history.push(`/detail-user/${idUser}`)
  }

  return (
    <>
      <CCard className="mb-4">
        <CCardBody>
          <h1>USER LIST</h1>
        {/* <CButton color="info" variant="outline" onClick={handleInsertUser}>Create User</CButton> */}
          <CTable hover>
            <CTableHead>
              <CTableRow>
                <CTableHeaderCell scope="col">No</CTableHeaderCell>
                <CTableHeaderCell scope="col">ID</CTableHeaderCell>
                <CTableHeaderCell scope="col">Name</CTableHeaderCell>
                <CTableHeaderCell scope="col">Age</CTableHeaderCell>
                <CTableHeaderCell scope="col">Action</CTableHeaderCell>
              </CTableRow>
            </CTableHead>
            <CTableBody>
              {userList !== null && (
                <>
                  {userList.map((val, index) => {
                    return (
                      <CTableRow key={index}>
                        <CTableHeaderCell scope="row">{index + 1}</CTableHeaderCell>
                        <CTableDataCell>{val.id}</CTableDataCell>
                        <CTableDataCell>{val.name}</CTableDataCell>
                        <CTableDataCell>{val.age}</CTableDataCell>
                        <CTableDataCell>
                          <CButton color="info" variant="ghost" onClick={handleDetail} value={val.id}>
                            Detail
                          </CButton>
                        </CTableDataCell>
                      </CTableRow>
                    )
                  })}
                </>
              )}
            </CTableBody>
          </CTable>
        </CCardBody>
      </CCard>
    </>
  )
}

export default ListUser